;(function () {
	'use strict';

	/**
	 * @preserve FastClick: polyfill to remove click delays on browsers with touch UIs.
	 *
	 * @codingstandard ftlabs-jsv2
	 * @copyright The Financial Times Limited [All Rights Reserved]
	 * @license MIT License (see LICENSE.txt)
	 */

	/*jslint browser:true, node:true*/
	/*global define, Event, Node*/


	/**
	 * Instantiate fast-clicking listeners on the specified layer.
	 *
	 * @constructor
	 * @param {Element} layer The layer to listen on
	 * @param {Object} [options={}] The options to override the defaults
	 */
	function FastClick(layer, options) {
		var oldOnClick;

		options = options || {};

		/**
		 * Whether a click is currently being tracked.
		 *
		 * @type boolean
		 */
		this.trackingClick = false;


		/**
		 * Timestamp for when click tracking started.
		 *
		 * @type number
		 */
		this.trackingClickStart = 0;


		/**
		 * The element being tracked for a click.
		 *
		 * @type EventTarget
		 */
		this.targetElement = null;


		/**
		 * X-coordinate of touch start event.
		 *
		 * @type number
		 */
		this.touchStartX = 0;


		/**
		 * Y-coordinate of touch start event.
		 *
		 * @type number
		 */
		this.touchStartY = 0;


		/**
		 * ID of the last touch, retrieved from Touch.identifier.
		 *
		 * @type number
		 */
		this.lastTouchIdentifier = 0;


		/**
		 * Touchmove boundary, beyond which a click will be cancelled.
		 *
		 * @type number
		 */
		this.touchBoundary = options.touchBoundary || 10;


		/**
		 * The FastClick layer.
		 *
		 * @type Element
		 */
		this.layer = layer;

		/**
		 * The minimum time between tap(touchstart and touchend) events
		 *
		 * @type number
		 */
		this.tapDelay = options.tapDelay || 200;

		/**
		 * The maximum time for a tap
		 *
		 * @type number
		 */
		this.tapTimeout = options.tapTimeout || 700;

		if (FastClick.notNeeded(layer)) {
			return;
		}

		// Some old versions of Android don't have Function.prototype.bind
		function bind(method, context) {
			return function() { return method.apply(context, arguments); };
		}


		var methods = ['onMouse', 'onClick', 'onTouchStart', 'onTouchMove', 'onTouchEnd', 'onTouchCancel'];
		var context = this;
		for (var i = 0, l = methods.length; i < l; i++) {
			context[methods[i]] = bind(context[methods[i]], context);
		}

		// Set up event handlers as required
		if (deviceIsAndroid) {
			layer.addEventListener('mouseover', this.onMouse, true);
			layer.addEventListener('mousedown', this.onMouse, true);
			layer.addEventListener('mouseup', this.onMouse, true);
		}

		layer.addEventListener('click', this.onClick, true);
		layer.addEventListener('touchstart', this.onTouchStart, false);
		layer.addEventListener('touchmove', this.onTouchMove, false);
		layer.addEventListener('touchend', this.onTouchEnd, false);
		layer.addEventListener('touchcancel', this.onTouchCancel, false);

		// Hack is required for browsers that don't support Event#stopImmediatePropagation (e.g. Android 2)
		// which is how FastClick normally stops click events bubbling to callbacks registered on the FastClick
		// layer when they are cancelled.
		if (!Event.prototype.stopImmediatePropagation) {
			layer.removeEventListener = function(type, callback, capture) {
				var rmv = Node.prototype.removeEventListener;
				if (type === 'click') {
					rmv.call(layer, type, callback.hijacked || callback, capture);
				} else {
					rmv.call(layer, type, callback, capture);
				}
			};

			layer.addEventListener = function(type, callback, capture) {
				var adv = Node.prototype.addEventListener;
				if (type === 'click') {
					adv.call(layer, type, callback.hijacked || (callback.hijacked = function(event) {
						if (!event.propagationStopped) {
							callback(event);
						}
					}), capture);
				} else {
					adv.call(layer, type, callback, capture);
				}
			};
		}

		// If a handler is already declared in the element's onclick attribute, it will be fired before
		// FastClick's onClick handler. Fix this by pulling out the user-defined handler function and
		// adding it as listener.
		if (typeof layer.onclick === 'function') {

			// Android browser on at least 3.2 requires a new reference to the function in layer.onclick
			// - the old one won't work if passed to addEventListener directly.
			oldOnClick = layer.onclick;
			layer.addEventListener('click', function(event) {
				oldOnClick(event);
			}, false);
			layer.onclick = null;
		}
	}

	/**
	* Windows Phone 8.1 fakes user agent string to look like Android and iPhone.
	*
	* @type boolean
	*/
	var deviceIsWindowsPhone = navigator.userAgent.indexOf("Windows Phone") >= 0;

	/**
	 * Android requires exceptions.
	 *
	 * @type boolean
	 */
	var deviceIsAndroid = navigator.userAgent.indexOf('Android') > 0 && !deviceIsWindowsPhone;


	/**
	 * iOS requires exceptions.
	 *
	 * @type boolean
	 */
	var deviceIsIOS = /iP(ad|hone|od)/.test(navigator.userAgent) && !deviceIsWindowsPhone;


	/**
	 * iOS 4 requires an exception for select elements.
	 *
	 * @type boolean
	 */
	var deviceIsIOS4 = deviceIsIOS && (/OS 4_\d(_\d)?/).test(navigator.userAgent);


	/**
	 * iOS 6.0-7.* requires the target element to be manually derived
	 *
	 * @type boolean
	 */
	var deviceIsIOSWithBadTarget = deviceIsIOS && (/OS [6-7]_\d/).test(navigator.userAgent);

	/**
	 * BlackBerry requires exceptions.
	 *
	 * @type boolean
	 */
	var deviceIsBlackBerry10 = navigator.userAgent.indexOf('BB10') > 0;

	/**
	 * Determine whether a given element requires a native click.
	 *
	 * @param {EventTarget|Element} target Target DOM element
	 * @returns {boolean} Returns true if the element needs a native click
	 */
	FastClick.prototype.needsClick = function(target) {
		switch (target.nodeName.toLowerCase()) {

		// Don't send a synthetic click to disabled inputs (issue #62)
		case 'button':
		case 'select':
		case 'textarea':
			if (target.disabled) {
				return true;
			}

			break;
		case 'input':

			// File inputs need real clicks on iOS 6 due to a browser bug (issue #68)
			if ((deviceIsIOS && target.type === 'file') || target.disabled) {
				return true;
			}

			break;
		case 'label':
		case 'iframe': // iOS8 homescreen apps can prevent events bubbling into frames
		case 'video':
			return true;
		}

		return (/\bneedsclick\b/).test(target.className);
	};


	/**
	 * Determine whether a given element requires a call to focus to simulate click into element.
	 *
	 * @param {EventTarget|Element} target Target DOM element
	 * @returns {boolean} Returns true if the element requires a call to focus to simulate native click.
	 */
	FastClick.prototype.needsFocus = function(target) {
		switch (target.nodeName.toLowerCase()) {
		case 'textarea':
			return true;
		case 'select':
			return !deviceIsAndroid;
		case 'input':
			switch (target.type) {
			case 'button':
			case 'checkbox':
			case 'file':
			case 'image':
			case 'radio':
			case 'submit':
				return false;
			}

			// No point in attempting to focus disabled inputs
			return !target.disabled && !target.readOnly;
		default:
			return (/\bneedsfocus\b/).test(target.className);
		}
	};


	/**
	 * Send a click event to the specified element.
	 *
	 * @param {EventTarget|Element} targetElement
	 * @param {Event} event
	 */
	FastClick.prototype.sendClick = function(targetElement, event) {
		var clickEvent, touch;

		// On some Android devices activeElement needs to be blurred otherwise the synthetic click will have no effect (#24)
		if (document.activeElement && document.activeElement !== targetElement) {
			document.activeElement.blur();
		}

		touch = event.changedTouches[0];

		// Synthesise a click event, with an extra attribute so it can be tracked
		clickEvent = document.createEvent('MouseEvents');
		clickEvent.initMouseEvent(this.determineEventType(targetElement), true, true, window, 1, touch.screenX, touch.screenY, touch.clientX, touch.clientY, false, false, false, false, 0, null);
		clickEvent.forwardedTouchEvent = true;
		targetElement.dispatchEvent(clickEvent);
	};

	FastClick.prototype.determineEventType = function(targetElement) {

		//Issue #159: Android Chrome Select Box does not open with a synthetic click event
		if (deviceIsAndroid && targetElement.tagName.toLowerCase() === 'select') {
			return 'mousedown';
		}

		return 'click';
	};


	/**
	 * @param {EventTarget|Element} targetElement
	 */
	FastClick.prototype.focus = function(targetElement) {
		var length;

		// Issue #160: on iOS 7, some input elements (e.g. date datetime month) throw a vague TypeError on setSelectionRange. These elements don't have an integer value for the selectionStart and selectionEnd properties, but unfortunately that can't be used for detection because accessing the properties also throws a TypeError. Just check the type instead. Filed as Apple bug #15122724.
		if (deviceIsIOS && targetElement.setSelectionRange && targetElement.type.indexOf('date') !== 0 && targetElement.type !== 'time' && targetElement.type !== 'month') {
			length = targetElement.value.length;
			targetElement.setSelectionRange(length, length);
		} else {
			targetElement.focus();
		}
	};


	/**
	 * Check whether the given target element is a child of a scrollable layer and if so, set a flag on it.
	 *
	 * @param {EventTarget|Element} targetElement
	 */
	FastClick.prototype.updateScrollParent = function(targetElement) {
		var scrollParent, parentElement;

		scrollParent = targetElement.fastClickScrollParent;

		// Attempt to discover whether the target element is contained within a scrollable layer. Re-check if the
		// target element was moved to another parent.
		if (!scrollParent || !scrollParent.contains(targetElement)) {
			parentElement = targetElement;
			do {
				if (parentElement.scrollHeight > parentElement.offsetHeight) {
					scrollParent = parentElement;
					targetElement.fastClickScrollParent = parentElement;
					break;
				}

				parentElement = parentElement.parentElement;
			} while (parentElement);
		}

		// Always update the scroll top tracker if possible.
		if (scrollParent) {
			scrollParent.fastClickLastScrollTop = scrollParent.scrollTop;
		}
	};


	/**
	 * @param {EventTarget} targetElement
	 * @returns {Element|EventTarget}
	 */
	FastClick.prototype.getTargetElementFromEventTarget = function(eventTarget) {

		// On some older browsers (notably Safari on iOS 4.1 - see issue #56) the event target may be a text node.
		if (eventTarget.nodeType === Node.TEXT_NODE) {
			return eventTarget.parentNode;
		}

		return eventTarget;
	};


	/**
	 * On touch start, record the position and scroll offset.
	 *
	 * @param {Event} event
	 * @returns {boolean}
	 */
	FastClick.prototype.onTouchStart = function(event) {
		var targetElement, touch, selection;

		// Ignore multiple touches, otherwise pinch-to-zoom is prevented if both fingers are on the FastClick element (issue #111).
		if (event.targetTouches.length > 1) {
			return true;
		}

		targetElement = this.getTargetElementFromEventTarget(event.target);
		touch = event.targetTouches[0];

		if (deviceIsIOS) {

			// Only trusted events will deselect text on iOS (issue #49)
			selection = window.getSelection();
			if (selection.rangeCount && !selection.isCollapsed) {
				return true;
			}

			if (!deviceIsIOS4) {

				// Weird things happen on iOS when an alert or confirm dialog is opened from a click event callback (issue #23):
				// when the user next taps anywhere else on the page, new touchstart and touchend events are dispatched
				// with the same identifier as the touch event that previously triggered the click that triggered the alert.
				// Sadly, there is an issue on iOS 4 that causes some normal touch events to have the same identifier as an
				// immediately preceeding touch event (issue #52), so this fix is unavailable on that platform.
				// Issue 120: touch.identifier is 0 when Chrome dev tools 'Emulate touch events' is set with an iOS device UA string,
				// which causes all touch events to be ignored. As this block only applies to iOS, and iOS identifiers are always long,
				// random integers, it's safe to to continue if the identifier is 0 here.
				if (touch.identifier && touch.identifier === this.lastTouchIdentifier) {
					event.preventDefault();
					return false;
				}

				this.lastTouchIdentifier = touch.identifier;

				// If the target element is a child of a scrollable layer (using -webkit-overflow-scrolling: touch) and:
				// 1) the user does a fling scroll on the scrollable layer
				// 2) the user stops the fling scroll with another tap
				// then the event.target of the last 'touchend' event will be the element that was under the user's finger
				// when the fling scroll was started, causing FastClick to send a click event to that layer - unless a check
				// is made to ensure that a parent layer was not scrolled before sending a synthetic click (issue #42).
				this.updateScrollParent(targetElement);
			}
		}

		this.trackingClick = true;
		this.trackingClickStart = event.timeStamp;
		this.targetElement = targetElement;

		this.touchStartX = touch.pageX;
		this.touchStartY = touch.pageY;

		// Prevent phantom clicks on fast double-tap (issue #36)
		if ((event.timeStamp - this.lastClickTime) < this.tapDelay) {
			event.preventDefault();
		}

		return true;
	};


	/**
	 * Based on a touchmove event object, check whether the touch has moved past a boundary since it started.
	 *
	 * @param {Event} event
	 * @returns {boolean}
	 */
	FastClick.prototype.touchHasMoved = function(event) {
		var touch = event.changedTouches[0], boundary = this.touchBoundary;

		if (Math.abs(touch.pageX - this.touchStartX) > boundary || Math.abs(touch.pageY - this.touchStartY) > boundary) {
			return true;
		}

		return false;
	};


	/**
	 * Update the last position.
	 *
	 * @param {Event} event
	 * @returns {boolean}
	 */
	FastClick.prototype.onTouchMove = function(event) {
		if (!this.trackingClick) {
			return true;
		}

		// If the touch has moved, cancel the click tracking
		if (this.targetElement !== this.getTargetElementFromEventTarget(event.target) || this.touchHasMoved(event)) {
			this.trackingClick = false;
			this.targetElement = null;
		}

		return true;
	};


	/**
	 * Attempt to find the labelled control for the given label element.
	 *
	 * @param {EventTarget|HTMLLabelElement} labelElement
	 * @returns {Element|null}
	 */
	FastClick.prototype.findControl = function(labelElement) {

		// Fast path for newer browsers supporting the HTML5 control attribute
		if (labelElement.control !== undefined) {
			return labelElement.control;
		}

		// All browsers under test that support touch events also support the HTML5 htmlFor attribute
		if (labelElement.htmlFor) {
			return document.getElementById(labelElement.htmlFor);
		}

		// If no for attribute exists, attempt to retrieve the first labellable descendant element
		// the list of which is defined here: http://www.w3.org/TR/html5/forms.html#category-label
		return labelElement.querySelector('button, input:not([type=hidden]), keygen, meter, output, progress, select, textarea');
	};


	/**
	 * On touch end, determine whether to send a click event at once.
	 *
	 * @param {Event} event
	 * @returns {boolean}
	 */
	FastClick.prototype.onTouchEnd = function(event) {
		var forElement, trackingClickStart, targetTagName, scrollParent, touch, targetElement = this.targetElement;

		if (!this.trackingClick) {
			return true;
		}

		// Prevent phantom clicks on fast double-tap (issue #36)
		if ((event.timeStamp - this.lastClickTime) < this.tapDelay) {
			this.cancelNextClick = true;
			return true;
		}

		if ((event.timeStamp - this.trackingClickStart) > this.tapTimeout) {
			return true;
		}

		// Reset to prevent wrong click cancel on input (issue #156).
		this.cancelNextClick = false;

		this.lastClickTime = event.timeStamp;

		trackingClickStart = this.trackingClickStart;
		this.trackingClick = false;
		this.trackingClickStart = 0;

		// On some iOS devices, the targetElement supplied with the event is invalid if the layer
		// is performing a transition or scroll, and has to be re-detected manually. Note that
		// for this to function correctly, it must be called *after* the event target is checked!
		// See issue #57; also filed as rdar://13048589 .
		if (deviceIsIOSWithBadTarget) {
			touch = event.changedTouches[0];

			// In certain cases arguments of elementFromPoint can be negative, so prevent setting targetElement to null
			targetElement = document.elementFromPoint(touch.pageX - window.pageXOffset, touch.pageY - window.pageYOffset) || targetElement;
			targetElement.fastClickScrollParent = this.targetElement.fastClickScrollParent;
		}

		targetTagName = targetElement.tagName.toLowerCase();
		if (targetTagName === 'label') {
			forElement = this.findControl(targetElement);
			if (forElement) {
				this.focus(targetElement);
				if (deviceIsAndroid) {
					return false;
				}

				targetElement = forElement;
			}
		} else if (this.needsFocus(targetElement)) {

			// Case 1: If the touch started a while ago (best guess is 100ms based on tests for issue #36) then focus will be triggered anyway. Return early and unset the target element reference so that the subsequent click will be allowed through.
			// Case 2: Without this exception for input elements tapped when the document is contained in an iframe, then any inputted text won't be visible even though the value attribute is updated as the user types (issue #37).
			if ((event.timeStamp - trackingClickStart) > 100 || (deviceIsIOS && window.top !== window && targetTagName === 'input')) {
				this.targetElement = null;
				return false;
			}

			this.focus(targetElement);
			this.sendClick(targetElement, event);

			// Select elements need the event to go through on iOS 4, otherwise the selector menu won't open.
			// Also this breaks opening selects when VoiceOver is active on iOS6, iOS7 (and possibly others)
			if (!deviceIsIOS || targetTagName !== 'select') {
				this.targetElement = null;
				event.preventDefault();
			}

			return false;
		}

		if (deviceIsIOS && !deviceIsIOS4) {

			// Don't send a synthetic click event if the target element is contained within a parent layer that was scrolled
			// and this tap is being used to stop the scrolling (usually initiated by a fling - issue #42).
			scrollParent = targetElement.fastClickScrollParent;
			if (scrollParent && scrollParent.fastClickLastScrollTop !== scrollParent.scrollTop) {
				return true;
			}
		}

		// Prevent the actual click from going though - unless the target node is marked as requiring
		// real clicks or if it is in the whitelist in which case only non-programmatic clicks are permitted.
		if (!this.needsClick(targetElement)) {
			event.preventDefault();
			this.sendClick(targetElement, event);
		}

		return false;
	};


	/**
	 * On touch cancel, stop tracking the click.
	 *
	 * @returns {void}
	 */
	FastClick.prototype.onTouchCancel = function() {
		this.trackingClick = false;
		this.targetElement = null;
	};


	/**
	 * Determine mouse events which should be permitted.
	 *
	 * @param {Event} event
	 * @returns {boolean}
	 */
	FastClick.prototype.onMouse = function(event) {

		// If a target element was never set (because a touch event was never fired) allow the event
		if (!this.targetElement) {
			return true;
		}

		if (event.forwardedTouchEvent) {
			return true;
		}

		// Programmatically generated events targeting a specific element should be permitted
		if (!event.cancelable) {
			return true;
		}

		// Derive and check the target element to see whether the mouse event needs to be permitted;
		// unless explicitly enabled, prevent non-touch click events from triggering actions,
		// to prevent ghost/doubleclicks.
		if (!this.needsClick(this.targetElement) || this.cancelNextClick) {

			// Prevent any user-added listeners declared on FastClick element from being fired.
			if (event.stopImmediatePropagation) {
				event.stopImmediatePropagation();
			} else {

				// Part of the hack for browsers that don't support Event#stopImmediatePropagation (e.g. Android 2)
				event.propagationStopped = true;
			}

			// Cancel the event
			event.stopPropagation();
			event.preventDefault();

			return false;
		}

		// If the mouse event is permitted, return true for the action to go through.
		return true;
	};


	/**
	 * On actual clicks, determine whether this is a touch-generated click, a click action occurring
	 * naturally after a delay after a touch (which needs to be cancelled to avoid duplication), or
	 * an actual click which should be permitted.
	 *
	 * @param {Event} event
	 * @returns {boolean}
	 */
	FastClick.prototype.onClick = function(event) {
		var permitted;

		// It's possible for another FastClick-like library delivered with third-party code to fire a click event before FastClick does (issue #44). In that case, set the click-tracking flag back to false and return early. This will cause onTouchEnd to return early.
		if (this.trackingClick) {
			this.targetElement = null;
			this.trackingClick = false;
			return true;
		}

		// Very odd behaviour on iOS (issue #18): if a submit element is present inside a form and the user hits enter in the iOS simulator or clicks the Go button on the pop-up OS keyboard the a kind of 'fake' click event will be triggered with the submit-type input element as the target.
		if (event.target.type === 'submit' && event.detail === 0) {
			return true;
		}

		permitted = this.onMouse(event);

		// Only unset targetElement if the click is not permitted. This will ensure that the check for !targetElement in onMouse fails and the browser's click doesn't go through.
		if (!permitted) {
			this.targetElement = null;
		}

		// If clicks are permitted, return true for the action to go through.
		return permitted;
	};


	/**
	 * Remove all FastClick's event listeners.
	 *
	 * @returns {void}
	 */
	FastClick.prototype.destroy = function() {
		var layer = this.layer;

		if (deviceIsAndroid) {
			layer.removeEventListener('mouseover', this.onMouse, true);
			layer.removeEventListener('mousedown', this.onMouse, true);
			layer.removeEventListener('mouseup', this.onMouse, true);
		}

		layer.removeEventListener('click', this.onClick, true);
		layer.removeEventListener('touchstart', this.onTouchStart, false);
		layer.removeEventListener('touchmove', this.onTouchMove, false);
		layer.removeEventListener('touchend', this.onTouchEnd, false);
		layer.removeEventListener('touchcancel', this.onTouchCancel, false);
	};


	/**
	 * Check whether FastClick is needed.
	 *
	 * @param {Element} layer The layer to listen on
	 */
	FastClick.notNeeded = function(layer) {
		var metaViewport;
		var chromeVersion;
		var blackberryVersion;
		var firefoxVersion;

		// Devices that don't support touch don't need FastClick
		if (typeof window.ontouchstart === 'undefined') {
			return true;
		}

		// Chrome version - zero for other browsers
		chromeVersion = +(/Chrome\/([0-9]+)/.exec(navigator.userAgent) || [,0])[1];

		if (chromeVersion) {

			if (deviceIsAndroid) {
				metaViewport = document.querySelector('meta[name=viewport]');

				if (metaViewport) {
					// Chrome on Android with user-scalable="no" doesn't need FastClick (issue #89)
					if (metaViewport.content.indexOf('user-scalable=no') !== -1) {
						return true;
					}
					// Chrome 32 and above with width=device-width or less don't need FastClick
					if (chromeVersion > 31 && document.documentElement.scrollWidth <= window.outerWidth) {
						return true;
					}
				}

			// Chrome desktop doesn't need FastClick (issue #15)
			} else {
				return true;
			}
		}

		if (deviceIsBlackBerry10) {
			blackberryVersion = navigator.userAgent.match(/Version\/([0-9]*)\.([0-9]*)/);

			// BlackBerry 10.3+ does not require Fastclick library.
			// https://github.com/ftlabs/fastclick/issues/251
			if (blackberryVersion[1] >= 10 && blackberryVersion[2] >= 3) {
				metaViewport = document.querySelector('meta[name=viewport]');

				if (metaViewport) {
					// user-scalable=no eliminates click delay.
					if (metaViewport.content.indexOf('user-scalable=no') !== -1) {
						return true;
					}
					// width=device-width (or less than device-width) eliminates click delay.
					if (document.documentElement.scrollWidth <= window.outerWidth) {
						return true;
					}
				}
			}
		}

		// IE10 with -ms-touch-action: none or manipulation, which disables double-tap-to-zoom (issue #97)
		if (layer.style.msTouchAction === 'none' || layer.style.touchAction === 'manipulation') {
			return true;
		}

		// Firefox version - zero for other browsers
		firefoxVersion = +(/Firefox\/([0-9]+)/.exec(navigator.userAgent) || [,0])[1];

		if (firefoxVersion >= 27) {
			// Firefox 27+ does not have tap delay if the content is not zoomable - https://bugzilla.mozilla.org/show_bug.cgi?id=922896

			metaViewport = document.querySelector('meta[name=viewport]');
			if (metaViewport && (metaViewport.content.indexOf('user-scalable=no') !== -1 || document.documentElement.scrollWidth <= window.outerWidth)) {
				return true;
			}
		}

		// IE11: prefixed -ms-touch-action is no longer supported and it's recomended to use non-prefixed version
		// http://msdn.microsoft.com/en-us/library/windows/apps/Hh767313.aspx
		if (layer.style.touchAction === 'none' || layer.style.touchAction === 'manipulation') {
			return true;
		}

		return false;
	};


	/**
	 * Factory method for creating a FastClick object
	 *
	 * @param {Element} layer The layer to listen on
	 * @param {Object} [options={}] The options to override the defaults
	 */
	FastClick.attach = function(layer, options) {
		return new FastClick(layer, options);
	};


	if (typeof define === 'function' && typeof define.amd === 'object' && define.amd) {

		// AMD. Register as an anonymous module.
		define(function() {
			return FastClick;
		});
	} else if (typeof module !== 'undefined' && module.exports) {
		module.exports = FastClick.attach;
		module.exports.FastClick = FastClick;
	} else {
		window.FastClick = FastClick;
	}
}());

/*!
loadCSS: load a CSS file asynchronously.
[c]2015 @scottjehl, Filament Group, Inc.
Licensed MIT
*/
(function(w){
	"use strict";
	/* exported loadCSS */
	var loadCSS = function( href, before, media ){
		// Arguments explained:
		// `href` [REQUIRED] is the URL for your CSS file.
		// `before` [OPTIONAL] is the element the script should use as a reference for injecting our stylesheet <link> before
			// By default, loadCSS attempts to inject the link after the last stylesheet or script in the DOM. However, you might desire a more specific location in your document.
		// `media` [OPTIONAL] is the media type or query of the stylesheet. By default it will be 'all'
		var doc = w.document;
		var ss = doc.createElement( "link" );
		var ref;
		if( before ){
			ref = before;
		}
		else {
			var refs = ( doc.body || doc.getElementsByTagName( "head" )[ 0 ] ).childNodes;
			ref = refs[ refs.length - 1];
		}

		var sheets = doc.styleSheets;
		ss.rel = "stylesheet";
		ss.href = href;
		// temporarily set media to something inapplicable to ensure it'll fetch without blocking render
		ss.media = "only x";

		// Inject link
			// Note: the ternary preserves the existing behavior of "before" argument, but we could choose to change the argument to "after" in a later release and standardize on ref.nextSibling for all refs
			// Note: `insertBefore` is used instead of `appendChild`, for safety re: http://www.paulirish.com/2011/surefire-dom-element-insertion/
		ref.parentNode.insertBefore( ss, ( before ? ref : ref.nextSibling ) );
		// A method (exposed on return object for external use) that mimics onload by polling until document.styleSheets until it includes the new sheet.
		var onloadcssdefined = function( cb ){
			var resolvedHref = ss.href;
			var i = sheets.length;
			while( i-- ){
				if( sheets[ i ].href === resolvedHref ){
					return cb();
				}
			}
			setTimeout(function() {
				onloadcssdefined( cb );
			});
		};

		// once loaded, set link's media back to `all` so that the stylesheet applies once it loads
		ss.onloadcssdefined = onloadcssdefined;
		onloadcssdefined(function() {
			ss.media = media || "all";
		});
		return ss;
	};
	// commonjs
	if( typeof module !== "undefined" ){
		module.exports = loadCSS;
	}
	else {
		w.loadCSS = loadCSS;
	}
}( typeof global !== "undefined" ? global : this ));


/*! loadJS: load a JS file asynchronously. [c]2014 @scottjehl, Filament Group, Inc. (Based on http://goo.gl/REQGQ by Paul Irish). Licensed MIT */
(function( w ){
	var loadJS = function( src, cb ){
		"use strict";
		var ref = w.document.getElementsByTagName( "script" )[ 0 ];
		var script = w.document.createElement( "script" );
		script.src = src;
		script.async = true;
		ref.parentNode.insertBefore( script, ref );
		if (cb && typeof(cb) === "function") {
			script.onload = cb;
		}
		return script;
	};
	// commonjs
	if( typeof module !== "undefined" ){
		module.exports = loadJS;
	}
	else {
		w.loadJS = loadJS;
	}
}( typeof global !== "undefined" ? global : this ));

/**
 * Utility functions.
 */

(function (window, document, app) {

  function Util() {
  }

  /**
   * Limits frequency of execution of the given function.
   * https://remysharp.com/2010/07/21/throttling-function-calls (Remy Sharp, MIT License)
   *
   * @param {Function} fn
   * @param {Number} delay
   * @returns {Function}
   */
  Util.prototype.debounce = function (fn, delay) {
    var timer = null;
    return function () {
      var context = this, args = arguments;
      clearTimeout(timer);
      timer = setTimeout(function () {
        fn.apply(context, args);
      }, delay);
    };
  };

  /**
   * Checks whether the given element has the given class.
   *
   * @param {Node} elm
   * @param {String} className
   * @returns {Boolean}
   */
  Util.prototype.hasClass = function (elm, className) {
    return (' ' + elm.className + ' ').indexOf(' ' + className + ' ') > -1;
  };

  /**
   * Checks if the given value is in the given array.
   *
   * @param {Array} array
   * @param {mixed} value
   * @returns {Number}
   */
  Util.prototype.indexOf = function (array, value) {
    for (var i = 0, il = array.length; i < il; i++) {
      if (value === array[i]) {
        return i;
      }
    }
    return -1;
  };

  /**
   * Returns a collection of nodes for the given HTML string.
   *
   * @param {String} html
   * @returns {NodeList}
   */
  Util.prototype.toNodes = function (html) {
    var parent = document.createElement('div');
    parent.innerHTML = html;
    return parent.childNodes;
  };

  /**
   * Inserts the given HTML string before the given element.
   *
   * @param {Node} elm
   * @param {String} html
   */
  Util.prototype.before = function (elm, html) {
    elm.insertAdjacentHTML('beforebegin', html);
  };

  /**
   * Inserts the given HTML string after the given element.
   *
   * @param {Node} elm
   * @param {String} html
   */
  Util.prototype.after = function (elm, html) {
    elm.insertAdjacentHTML('afterend', html);
  };

  /**
   * Inserts the given element at the begining of the given parent element.
   *
   * @param {Node} parentElm
   * @param {Node} elm
   */
  Util.prototype.prepend = function (parentElm, elm) {
    parentElm.insertBefore(elm, parentElm.firstChild);
  };

  /**
   * Inserts the given element at the end of the given parent element.
   *
   * @param {Node} parentElm
   * @param {Node} elm
   */
  Util.prototype.append = function (parentElm, elm) {
    parentElm.appendChild(elm);
  };

  /**
   * Removes the given element from the DOM.
   *
   * @param {Node} elm
   */
  Util.prototype.remove = function (elm) {
    elm.parentNode.removeChild(elm);
  };

  /**
   * Returns the text content of the given element.
   * https://github.com/jquery/sizzle/blob/master/src/sizzle.js  (jQuery Foundation, MIT License)
   *
   * @param {Node} elm
   * @returns {String}
   */
  Util.prototype.text = function (elm) {
    var node,
      ret = '',
      i = 0,
      nodeType = elm.nodeType;

    if (!nodeType) {
      // If no nodeType, this is expected to be an array
      while ((node = elm[i++])) {
        // Do not traverse comment nodes
        ret += this.text(node);
      }
    }
    else if (nodeType === 1 || nodeType === 9 || nodeType === 11) {
      // Use textContent for elements
      // innerText usage removed for consistency of new lines (jQuery #11153)
      if (typeof elm.textContent === 'string') {
        return elm.textContent;
      }
      else {
        // Traverse its children
        for (elm = elm.firstChild; elm; elm = elm.nextSibling) {
          ret += this.text(elm);
        }
      }
    }
    else if (nodeType === 3 || nodeType === 4) {
      return elm.nodeValue;
    }

    return ret;
  };

  window.util = new Util();

})(window, document);

/**
 * DOM ready state.
 *
 * Based on https://github.com/jquery/jquery/blob/master/src/core/ready.js (jQuery Foundation, MIT License)
 */

(function (window, document, util, undefined) {

  var flags = {
    // @todo Check for function instead (see lodash)
    addEvent: !!document.addEventListener, // Whether the browser supports addEventListener().
    ready: false, // Whether the DOM is ready.
    waiting: false // Whether we are waiting.
  };

  var queue = [];

  /**
   * Calls and removes functions from queue (FIFO).
   */
  function runQueue() {
    while (queue.length) {
      (queue.shift())();
    }
  }

  /**
   * Waits for the DOM to be ready.
   */
  function wait() {
    if (flags.waiting) {
      return;
    }

    flags.waiting = true;

    // If the DOM is already ready, then run queue.
    if (document.readyState === 'complete') {
      flags.ready = true;
      flags.waiting = false;

      runQueue();
    }
    // If the DOM is busy loading, then use event callback.
    else if (flags.addEvent) {
      document.addEventListener('DOMContentLoaded', complete, false);
      window.addEventListener('load', complete, false);
    }
    else {
      document.attachEvent('onreadystatechange', complete);
      window.attachEvent('onload', complete);
    }
  }

  /**
   * Callback fired after the DOM is ready.
   */
  function complete() {
    if (flags.addEvent) {
      document.removeEventListener('DOMContentLoaded', complete, false);
      window.removeEventListener('load', complete, false);
    }
    else {
      document.detachEvent('onreadystatechange', complete);
      window.detachEvent('onload', complete);
    }

    flags.ready = true;
    flags.waiting = false;

    runQueue();
  }

  /**
   *
   * @param {Function} fn Callback to fire once the DOM is ready.
   */
  util.ready = function (fn) {
    queue.push(fn);

    if (flags.ready) {
      runQueue();
    }
    else {
      wait();
    }
  };

})(window, document, window.util);

/**
 * Breakpoint handling.
 *
 * Based on https://github.com/netbek/rh-breakpoint (Hein Bekker, AGPLv3)
 */

(function (window, util, undefined) {

  var events = {};
  var doc = window.document;
  var docElm = doc.documentElement;
  var vwCache = {};
  var units = {};

  /**
   * Returns 1em in css px for html/body default size
   *
   * @returns {Number}
   *
   * Based on:
   * https://github.com/scottjehl/Respond
   */
  var getEmValue = (function () {
    var eminpx;
    // baseStyle also used by getEmValue (i.e.: width: 1em is important)
    var baseStyle = "position:absolute;left:0;visibility:hidden;display:block;padding:0;border:none;font-size:1em;width:1em;overflow:hidden;clip:rect(0px, 0px, 0px, 0px)";
    var fsCss = "font-size:100%!important;";
    return function () {
      var body;
      if (!eminpx && (body = doc.body)) {
        var div = doc.createElement("div"),
          originalHTMLCSS = docElm.style.cssText,
          originalBodyCSS = body.style.cssText;
        div.style.cssText = baseStyle;
        // 1em in a media query is the value of the default font size of the browser
        // reset docElem and body to ensure the correct value is returned
        docElm.style.cssText = fsCss;
        body.style.cssText = fsCss;
        body.appendChild(div);
        eminpx = div.offsetWidth;
        body.removeChild(div);
        //also update eminpx before returning
        eminpx = parseFloat(eminpx, 10);
        // restore the original values
        docElm.style.cssText = originalHTMLCSS;
        body.style.cssText = originalBodyCSS;
      }
      return eminpx || 16;
    };
  })();

  /**
   * Creates CSS units and invalidates the viewport depending cache
   *
   * Based on:
   * https://github.com/aFarkas/picturefill/blob/issues-313-262-314/src/picturefill.js
   */
  function updateUnits() {
    vwCache = {};
    units = {
      px: 1,
      width: Math.max(window.innerWidth || 0, docElm.clientWidth),
      height: Math.max(window.innerHeight || 0, docElm.clientHeight),
      em: getEmValue(),
      rem: getEmValue()
    };
    units.vw = units.width / 100;
    units.vh = units.height / 100;
    units.orientation = units.height > units.width; // If portrait
    units.devicePixelRatio = window.devicePixelRatio || 1;
    units.dpi = 0; // @todo
    units.dppx = 0; // @todo
    units.resolution = 0; // @todo
  }

  /**
   * Gets a mediaquery and returns a boolean or gets a CSS length and returns a number
   *
   * @param {String} css mediaqueries or css length
   * @returns {Boolean|Number}
   *
   * Based on:
   * https://gist.github.com/jonathantneal/db4f77009b155f083738
   * https://github.com/aFarkas/picturefill/blob/issues-313-262-314/src/picturefill.js
   */
  var evalCSS = (function () {
    var cache = {};
    var regLength = /^([\d\.]+)(em|vw|px)$/;
    var replace = function () {
      var args = arguments, index = 0, string = args[0];
      while (++index in args) {
        string = string.replace(args[index], args[++index]);
      }
      return string;
    };
    var buildStr = function (css) {
      if (!cache[css]) {
        cache[css] = "return " + replace((css || "").toLowerCase(),
          // ignore `only`
          /^only\s+/g, '',
          // wrap `not`
          /not([^)]+)/g, '!($1)',
          // strip vendor prefixes
          /-(webkit|moz|ms|o)-([a-z-]+)/g, '$2',
          /(min|max)--(webkit|moz|ms|o)-([a-z-]+)/g, '$1-$3',
          // interpret `all`, `portrait`, and `screen` as truthy
          /all|portrait|screen/g, 1,
          // interpret `landscape`, `print` and `speech` as falsey
          /landscape|print|speech/g, 0,
          // interpret `and`
          /\band\b/g, "&&",
          // interpret `,`
          /,/g, "||",
          // interpret `min-` as >=
          /min-device-pixel-ratio\s*:/g, 'e.devicePixelRatio >=',
          // interpret `max-` as >=
          /max-device-pixel-ratio\s*:/g, 'e.devicePixelRatio <=',
          // interpret as ==
          /device-pixel-ratio\s*:/g, 'e.devicePixelRatio ==',
          // interpret as ==
          /orientation\s*:/g, 'e.orientation ==',
          // interpret `min-` as >=
          /min-([a-z-\s]+):/g, "e.$1 >=",
          // interpret `min-` as <=
          /max-([a-z-\s]+):/g, "e.$1 <=",
          // calc value
          /calc([^)]+)/g, "($1)",
          // interpret css values
          /(\d+[\.]*[\d]*)([a-z]+)/g, "($1 * e.$2)"
          // make eval less evil - does not work?
//						/^(?!(e.[a-z]|[0-9\.&=|><\+\-\*\(\)\/])).*/ig, ""
          ) + ";";
        // Until we can strip invalid properties successfully to
        // make eval less evil, return nothing if `-` found. This
        // excludes vendor prefixes, for example.
        if (cache[css].indexOf('-') > -1) {
          cache[css] = 'return;';
        }
      }
      return cache[css];
    };

    return function (css, length) {
      var parsedLength;
      if (!(css in vwCache)) {
        vwCache[css] = false;
        if (length && (parsedLength = css.match(regLength))) {
          vwCache[css] = parsedLength[ 1 ] * units[parsedLength[ 2 ]];
        } else {
          /*jshint evil:true */
          try {
            vwCache[css] = new Function("e", buildStr(css))(units);
          } catch (e) {
          }
          /*jshint evil:false */
        }
      }
      return vwCache[css];
    };
  })();

  var matchesMedia;
  if (window.matchMedia && (matchMedia("(min-width: 0.1em)") || {}).matches) {
    matchesMedia = function (media) {
      return window.matchMedia(media).matches;
    };
  }
  else {
    matchesMedia = evalCSS;
  }

  /**
   *
   * @param {Object} mqs
   * @returns {Array}
   */
  util.breakpoint = function (mqs) {
    var matches = [];

    updateUnits();

    for (var name in mqs) {
      if (matchesMedia(mqs[name])) {
        matches.push(name);
      }
    }

    return matches;
  };

  /**
   *
   * @param {Object} mqs
   * @param {String} breakpoint
   * @returns {Boolean}
   */
  util.breakpointIncludes = function (mqs, breakpoint) {
    return util.indexOf(this.breakpoint(mqs), breakpoint) > -1;
  };

})(window, window.util);

(function (window, document, util, undefined) {

  /**
   *
   * @returns {AppConfig}
   */
  function AppConfig() {
    this.config = {
      // Same media queries as Foundation.
      mediaqueries: {
        medium: 'only screen and (min-width: 48em)',
        large: 'only screen and (min-width: 64em)'
      }
    };
  }

  /**
   *
   * @returns {Object}
   */
  AppConfig.prototype.get = function () {
    return this.config;
  };

  var flags = {
    tablet: false // Whether at tablet breakpoint or greater (determines whether table of contents is in DOM)
  };

  var $content, $toc;

  /**
   *
   * @returns {App}
   */
  function App() {
  }

  /**
   *
   */
  App.prototype.init = function () {
    $content = findContent();

    // Find headings for table of contents.
    var $headings = [];
    findHeadings(document.body, $headings);

    // Build HTML.
    var html = '', elm, depth, currentDepth = 0, fragments = {}, fragment, id, title, i, il = $headings.length;

    if (il) {
      html = '<nav role="navigation" class="toc wy-nav-side"><div class="wy-side-scroll"><h2 class="title">' + document.title + '</h2>';

      for (i = 0; i < il; i++) {
        elm = $headings[i];
        depth = Number(elm.tagName[1]);

        // If first child or if depth has increased, open new list.
        if (i === 0 || depth > currentDepth) {
          html += '<ol>';
          currentDepth = depth;
        }
        // If depth has decreased, close list.
        else if (depth < currentDepth) {
          html += '</ol>';
          currentDepth = depth;
        }

        title = util.text(elm);

        // Create fragment from lower kebab case of title.
        fragment = title.toLowerCase().replace(/[^\s\w]/gi, '').replace(/[\s_]/g, '-');

        // Set default fragment if needed.
        if (!fragment.length) {
          fragment = 'section';
        }

        // Check for duplicate fragments.
        if (fragment in fragments) {
          fragments[fragment]++;
          id = fragment + '-' + fragments[fragment];
        }
        else {
          fragments[fragment] = 1;
          id = fragment;
        }

        // Add ID to section title element.
        elm.setAttribute('id', id);

        html += '<li><a href="#' + id + '">' + title + '</a></li>';

        // If last child, close list.
        if (i === il - 1) {
          html += '</ol>';
        }
      }

      html += '</div></nav>';
    }

    $toc = util.toNodes(html)[0];

    // Set the initial state.
    resize(true);

    var debouncedResize = util.debounce(function (event) {
      resize();
    }, 60);

    // Add resize event listener.
    if (window.addEventListener) {
      window.addEventListener('resize', debouncedResize, false);
    }
    else {
      window.attachEvent('resize', debouncedResize);
    }
  };

  /**
   *
   * @returns {Boolean}
   */
  App.prototype.isTablet = function () {
    return util.breakpointIncludes(window.appConfig.get().mediaqueries, 'medium');
  };

  /**
   * Returns the content element.
   *
   * @returns {Node}
   */
  function findContent() {
    for (var elm = document.body.firstChild; elm !== null; elm = elm.nextSibling) {
      if (util.hasClass(elm, 'section')) {
        return elm;
      }
    }
  }

  /**
   * Returns heading elements for table of contents.
   *
   * @param {Node} parentElm
   * @param {Array} headings
   */
  function findHeadings(parentElm, headings) {
    // Ignore heading elements that are inside work steps.
    if (util.hasClass(parentElm, 'workstep')) {
      return;
    }

    for (var elm = parentElm.firstChild; elm !== null; elm = elm.nextSibling) {
      if (elm.nodeType !== 1) {
        continue;
      }

      if (elm.tagName.length === 2 && elm.tagName.match(/h[0-6]/i)) {
        headings.push(elm);
      }
      else {
        findHeadings(elm, headings);
      }
    }
  }

  /**
   * Callback fired on window resize.
   *
   * @param {Boolean} forceChange Whether to force a breakpoint change. Used to set initial state.
   */
  function resize(forceChange) {
    var isTablet = app.isTablet();

    if (forceChange || flags.tablet !== isTablet) {
      flags.tablet = isTablet;

      if ($toc) {
        if (isTablet) {
          util.prepend(document.body, $toc);
        }
        else {
          util.remove($toc);
        }
      }
    }
  }

  var app = new App();
  var appConfig = new AppConfig;

  window.app = app;
  window.appConfig = appConfig;

})(window, document, window.util);

/**
 * Bootstrap.
 */

(function (window, document, app, util, undefined) {

  var flags = {
    init: false // Whether init() has been fired.
  };

  var delay = 10, fnTimeout = setTimeout(fn, delay);

  // Fires init() when the DOM is ready.
  util.ready(function () {
    clearTimeout(fnTimeout);

    init();
  });

  /**
   * Fires init() when document.body exists.
   */
  function fn() {
    clearTimeout(fnTimeout);

    if (document.body) {
      init();
    }
    else {
      fnTimeout = setTimeout(fn, delay);
    }
  }

  /**
   * Initializes the app, which displays the table of contents, among other
   * things. To make the table of contents appear as quickly as possible, this
   * function is fired as soon as either document.body exists or any DOM ready
   * events (document.readystatechange, document.DOMContentLoaded, window.load)
   * have been fired. Saves ~40-100ms in Firefox 38; no savings in Chrome 46.
   */
  function init() {
    if (flags.init) {
      return;
    }

    flags.init = true;

    // Init app.
    app.init();

    // Init FastClick.
    FastClick.attach(document.body);

    // Allows math to be specified in TeX or LaTeX notation, with the AMSmath and AMSsymbols packages included, and produces output using the HTML-CSS output processor.
    // http://docs.mathjax.org/en/latest/configuration.html#using-a-configuration-file
    loadJS('bower_components/MathJax/MathJax.js?config=TeX-AMS_HTML');

//    if (app.isTablet()) {
//      // Load custom fonts asynchronously to prevent empty space while fonts are loading.
//      loadCSS('css/fonts.min.css');
//    }
  }

})(window, document, window.app, window.util);
