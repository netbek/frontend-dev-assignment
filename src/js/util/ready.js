/**
 * DOM ready state.
 *
 * Based on https://github.com/jquery/jquery/blob/master/src/core/ready.js (jQuery Foundation, MIT License)
 */

(function (window, document, util, undefined) {

  var flags = {
    // @todo Check for function instead (see lodash)
    addEvent: !!document.addEventListener, // Whether the browser supports addEventListener().
    ready: false, // Whether the DOM is ready.
    waiting: false // Whether we are waiting.
  };

  var queue = [];

  /**
   * Calls and removes functions from queue (FIFO).
   */
  function runQueue() {
    while (queue.length) {
      (queue.shift())();
    }
  }

  /**
   * Waits for the DOM to be ready.
   */
  function wait() {
    if (flags.waiting) {
      return;
    }

    flags.waiting = true;

    // If the DOM is already ready, then run queue.
    if (document.readyState === 'complete') {
      flags.ready = true;
      flags.waiting = false;

      runQueue();
    }
    // If the DOM is busy loading, then use event callback.
    else if (flags.addEvent) {
      document.addEventListener('DOMContentLoaded', complete, false);
      window.addEventListener('load', complete, false);
    }
    else {
      document.attachEvent('onreadystatechange', complete);
      window.attachEvent('onload', complete);
    }
  }

  /**
   * Callback fired after the DOM is ready.
   */
  function complete() {
    if (flags.addEvent) {
      document.removeEventListener('DOMContentLoaded', complete, false);
      window.removeEventListener('load', complete, false);
    }
    else {
      document.detachEvent('onreadystatechange', complete);
      window.detachEvent('onload', complete);
    }

    flags.ready = true;
    flags.waiting = false;

    runQueue();
  }

  /**
   *
   * @param {Function} fn Callback to fire once the DOM is ready.
   */
  util.ready = function (fn) {
    queue.push(fn);

    if (flags.ready) {
      runQueue();
    }
    else {
      wait();
    }
  };

})(window, document, window.util);
