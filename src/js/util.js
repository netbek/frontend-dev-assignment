/**
 * Utility functions.
 */

(function (window, document, app) {

  function Util() {
  }

  /**
   * Limits frequency of execution of the given function.
   * https://remysharp.com/2010/07/21/throttling-function-calls (Remy Sharp, MIT License)
   *
   * @param {Function} fn
   * @param {Number} delay
   * @returns {Function}
   */
  Util.prototype.debounce = function (fn, delay) {
    var timer = null;
    return function () {
      var context = this, args = arguments;
      clearTimeout(timer);
      timer = setTimeout(function () {
        fn.apply(context, args);
      }, delay);
    };
  };

  /**
   * Checks whether the given element has the given class.
   *
   * @param {Node} elm
   * @param {String} className
   * @returns {Boolean}
   */
  Util.prototype.hasClass = function (elm, className) {
    return (' ' + elm.className + ' ').indexOf(' ' + className + ' ') > -1;
  };

  /**
   * Checks if the given value is in the given array.
   *
   * @param {Array} array
   * @param {mixed} value
   * @returns {Number}
   */
  Util.prototype.indexOf = function (array, value) {
    for (var i = 0, il = array.length; i < il; i++) {
      if (value === array[i]) {
        return i;
      }
    }
    return -1;
  };

  /**
   * Returns a collection of nodes for the given HTML string.
   *
   * @param {String} html
   * @returns {NodeList}
   */
  Util.prototype.toNodes = function (html) {
    var parent = document.createElement('div');
    parent.innerHTML = html;
    return parent.childNodes;
  };

  /**
   * Inserts the given HTML string before the given element.
   *
   * @param {Node} elm
   * @param {String} html
   */
  Util.prototype.before = function (elm, html) {
    elm.insertAdjacentHTML('beforebegin', html);
  };

  /**
   * Inserts the given HTML string after the given element.
   *
   * @param {Node} elm
   * @param {String} html
   */
  Util.prototype.after = function (elm, html) {
    elm.insertAdjacentHTML('afterend', html);
  };

  /**
   * Inserts the given element at the begining of the given parent element.
   *
   * @param {Node} parentElm
   * @param {Node} elm
   */
  Util.prototype.prepend = function (parentElm, elm) {
    parentElm.insertBefore(elm, parentElm.firstChild);
  };

  /**
   * Inserts the given element at the end of the given parent element.
   *
   * @param {Node} parentElm
   * @param {Node} elm
   */
  Util.prototype.append = function (parentElm, elm) {
    parentElm.appendChild(elm);
  };

  /**
   * Removes the given element from the DOM.
   *
   * @param {Node} elm
   */
  Util.prototype.remove = function (elm) {
    elm.parentNode.removeChild(elm);
  };

  /**
   * Returns the text content of the given element.
   * https://github.com/jquery/sizzle/blob/master/src/sizzle.js  (jQuery Foundation, MIT License)
   *
   * @param {Node} elm
   * @returns {String}
   */
  Util.prototype.text = function (elm) {
    var node,
      ret = '',
      i = 0,
      nodeType = elm.nodeType;

    if (!nodeType) {
      // If no nodeType, this is expected to be an array
      while ((node = elm[i++])) {
        // Do not traverse comment nodes
        ret += this.text(node);
      }
    }
    else if (nodeType === 1 || nodeType === 9 || nodeType === 11) {
      // Use textContent for elements
      // innerText usage removed for consistency of new lines (jQuery #11153)
      if (typeof elm.textContent === 'string') {
        return elm.textContent;
      }
      else {
        // Traverse its children
        for (elm = elm.firstChild; elm; elm = elm.nextSibling) {
          ret += this.text(elm);
        }
      }
    }
    else if (nodeType === 3 || nodeType === 4) {
      return elm.nodeValue;
    }

    return ret;
  };

  window.util = new Util();

})(window, document);
