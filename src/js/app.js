(function (window, document, util, undefined) {

  /**
   *
   * @returns {AppConfig}
   */
  function AppConfig() {
    this.config = {
      // Same media queries as Foundation.
      mediaqueries: {
        medium: 'only screen and (min-width: 48em)',
        large: 'only screen and (min-width: 64em)'
      }
    };
  }

  /**
   *
   * @returns {Object}
   */
  AppConfig.prototype.get = function () {
    return this.config;
  };

  var flags = {
    tablet: false // Whether at tablet breakpoint or greater (determines whether table of contents is in DOM)
  };

  var $content, $toc;

  /**
   *
   * @returns {App}
   */
  function App() {
  }

  /**
   *
   */
  App.prototype.init = function () {
    $content = findContent();

    // Find headings for table of contents.
    var $headings = [];
    findHeadings(document.body, $headings);

    // Build HTML.
    var html = '', elm, depth, currentDepth = 0, fragments = {}, fragment, id, title, i, il = $headings.length;

    if (il) {
      html = '<nav role="navigation" class="toc wy-nav-side"><div class="wy-side-scroll"><h2 class="title">' + document.title + '</h2>';

      for (i = 0; i < il; i++) {
        elm = $headings[i];
        depth = Number(elm.tagName[1]);

        // If first child or if depth has increased, open new list.
        if (i === 0 || depth > currentDepth) {
          html += '<ol>';
          currentDepth = depth;
        }
        // If depth has decreased, close list.
        else if (depth < currentDepth) {
          html += '</ol>';
          currentDepth = depth;
        }

        title = util.text(elm);

        // Create fragment from lower kebab case of title.
        fragment = title.toLowerCase().replace(/[^\s\w]/gi, '').replace(/[\s_]/g, '-');

        // Set default fragment if needed.
        if (!fragment.length) {
          fragment = 'section';
        }

        // Check for duplicate fragments.
        if (fragment in fragments) {
          fragments[fragment]++;
          id = fragment + '-' + fragments[fragment];
        }
        else {
          fragments[fragment] = 1;
          id = fragment;
        }

        // Add ID to section title element.
        elm.setAttribute('id', id);

        html += '<li><a href="#' + id + '">' + title + '</a></li>';

        // If last child, close list.
        if (i === il - 1) {
          html += '</ol>';
        }
      }

      html += '</div></nav>';
    }

    $toc = util.toNodes(html)[0];

    // Set the initial state.
    resize(true);

    var debouncedResize = util.debounce(function (event) {
      resize();
    }, 60);

    // Add resize event listener.
    if (window.addEventListener) {
      window.addEventListener('resize', debouncedResize, false);
    }
    else {
      window.attachEvent('resize', debouncedResize);
    }
  };

  /**
   *
   * @returns {Boolean}
   */
  App.prototype.isTablet = function () {
    return util.breakpointIncludes(window.appConfig.get().mediaqueries, 'medium');
  };

  /**
   * Returns the content element.
   *
   * @returns {Node}
   */
  function findContent() {
    for (var elm = document.body.firstChild; elm !== null; elm = elm.nextSibling) {
      if (util.hasClass(elm, 'section')) {
        return elm;
      }
    }
  }

  /**
   * Returns heading elements for table of contents.
   *
   * @param {Node} parentElm
   * @param {Array} headings
   */
  function findHeadings(parentElm, headings) {
    // Ignore heading elements that are inside work steps.
    if (util.hasClass(parentElm, 'workstep')) {
      return;
    }

    for (var elm = parentElm.firstChild; elm !== null; elm = elm.nextSibling) {
      if (elm.nodeType !== 1) {
        continue;
      }

      if (elm.tagName.length === 2 && elm.tagName.match(/h[0-6]/i)) {
        headings.push(elm);
      }
      else {
        findHeadings(elm, headings);
      }
    }
  }

  /**
   * Callback fired on window resize.
   *
   * @param {Boolean} forceChange Whether to force a breakpoint change. Used to set initial state.
   */
  function resize(forceChange) {
    var isTablet = app.isTablet();

    if (forceChange || flags.tablet !== isTablet) {
      flags.tablet = isTablet;

      if ($toc) {
        if (isTablet) {
          util.prepend(document.body, $toc);
        }
        else {
          util.remove($toc);
        }
      }
    }
  }

  var app = new App();
  var appConfig = new AppConfig;

  window.app = app;
  window.appConfig = appConfig;

})(window, document, window.util);
