/**
 * Bootstrap.
 */

(function (window, document, app, util, undefined) {

  var flags = {
    init: false // Whether init() has been fired.
  };

  var delay = 10, fnTimeout = setTimeout(fn, delay);

  // Fires init() when the DOM is ready.
  util.ready(function () {
    clearTimeout(fnTimeout);

    init();
  });

  /**
   * Fires init() when document.body exists.
   */
  function fn() {
    clearTimeout(fnTimeout);

    if (document.body) {
      init();
    }
    else {
      fnTimeout = setTimeout(fn, delay);
    }
  }

  /**
   * Initializes the app, which displays the table of contents, among other
   * things. To make the table of contents appear as quickly as possible, this
   * function is fired as soon as either document.body exists or any DOM ready
   * events (document.readystatechange, document.DOMContentLoaded, window.load)
   * have been fired. Saves ~40-100ms in Firefox 38; no savings in Chrome 46.
   */
  function init() {
    if (flags.init) {
      return;
    }

    flags.init = true;

    // Init app.
    app.init();

    // Init FastClick.
    FastClick.attach(document.body);

    // Allows math to be specified in TeX or LaTeX notation, with the AMSmath and AMSsymbols packages included, and produces output using the HTML-CSS output processor.
    // http://docs.mathjax.org/en/latest/configuration.html#using-a-configuration-file
    loadJS('bower_components/MathJax/MathJax.js?config=TeX-AMS_HTML');

//    if (app.isTablet()) {
//      // Load custom fonts asynchronously to prevent empty space while fonts are loading.
//      loadCSS('css/fonts.min.css');
//    }
  }

})(window, document, window.app, window.util);
